# cds-tb-proxy

To run apps locally you need to:

1. Install [mitmproxy](https://mitmproxy.org/)

2. Install [mitmproxy CA certificate](https://docs.mitmproxy.org/stable/concepts-certificates/)

3. To turn on mitmproxy run the following command:
```
mitmdump -s apps-proxy.py
``` 

4. Please make sure that you set proxy to `127.0.0.1` and port `8080`,
here are instructions how to do that on [Windows](https://www.howtogeek.com/tips/how-to-set-your-proxy-settings-in-windows-8.1/) and [Mac](https://www.howtogeek.com/293444/how-to-configure-a-proxy-server-on-a-mac/)

5. Add `127.0.0.1 su-gdldev.gdldev-astack-tumorboard.localhost.rginger.com` to the hosts file (C:\Windows\System32\drivers\etc\hosts on Windows)

6. Open Chrome with the following flags:
```
"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe" --user-data-dir="C://chrome_dev_session" --disable-web-security
```

7. Open [TB url] (https://su-gdldev.gdldev-astack-tumorboard.cds.rginger.com/) - mitmproxy should replace urls of apps to the local ones.

8. If you run apps without `https`:
* click the "shield" icon on the right side of the address bar,
* in the dialog box, click "Load unsafe scripts".

9. In case of local domain change please update hosts file and following line:
```
self.localhost_domain = 'http://su-gdldev.gdldev-astack-tumorboard.localhost.rginger.com'
```
in apps-proxy.py` file

