from mitmproxy import ctx
import json


class Apps:
    def __init__(self):
        self.localhost_domain = 'http://su-gdldev.gdldev-astack-tumorboard.localhost.rginger.com'

    def response(self, flow):
        request = flow.request
        response = flow.response

        # replace apps urls
        if request.url.endswith("tumor-board/api/v1/configuration"):
            body = json.loads(response.content.decode("utf-8"))
            for index, app_config in enumerate(body["apps"]):
                if app_config["name"] == "CTM":
                    body["apps"][index]["url"] = self.localhost_domain + ":4500/app-manifest.json"
                if app_config["name"] == "PubSearch":
                    body["apps"][index]["url"] = self.localhost_domain + ":4600/app-manifest.json"
                if app_config["name"] == "GDL":
                    body["apps"][index]["url"] = self.localhost_domain + ":4700/app-manifest.json"

            response.content = json.dumps(body).encode("utf-8")

    def request(context, flow):
        request = flow.request

        # replace login background to easy see that the proxy is working
        if request.url.find("login-bg") and request.url.endswith(".svg"):
            request.host = "code4life.roche.com"
            request.path = "/_assets/images/share.jpg"

addons = [
    Apps()
]